<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnGroupsToUsersTable extends Migration
{
    /**
     * Run the migrations.
     * usersテーブルに対し、groupsカラムを追加
     * @return void
     */
    public function up()
    {
        // 対象DB: lead_system
        // 対象TBL: users
        // 追加カラム名: groups(VARCHAR型)
        // デフォルト値: 'reviewer'
        // idカラムの後に追加

        Schema::table('users', function ($table) {
            $table->string('groups')->after('id')->default('reviewer');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
