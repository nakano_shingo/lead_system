$(function(){
    /**
     * ■ Squareコンポーネント
     * ┗ React.Componentを継承するクラスを定義しない、「stateless functional components」と呼ばれるより簡易的な構文
     *   (renderメソッドのみからなるコンポーネントタイプ)
     **/
    function Square(props) {
        return (
            <button className="square" onClick={() => props.onClick()}>
                {props.value}
            </button>
        );
    }

    /**
     * ■ Boadコンポーネント
     * ┗ 9つの四角いマスをレンダリングする
     **/
    class Board extends React.Component {
        // マスを生成する
        makeBoardColumns() {
            // マス番号
            var squareCnt = 0;

            // 列を作成
            var columns = [];
            for (var c=0; c < 3; c++) {
                // 行を作成
                var rows = [];
                for (var r=0; r < 3; r++) {
                    rows.push(
                        this.renderSquare(squareCnt)
                    );
                    squareCnt++;
                }
                columns.push(
                    <div className="board-row" key={c}>
                        {rows}
                    </div>
                );
            }
            return (columns);
        }

        // 1マスを作成
        renderSquare(i) {
            return <Square value={this.props.squares[i]} onClick={() => this.props.onClick(i)} key={i} />;
        }

        render() {
            return (
                <div>
                    <div className="status">{status}</div>
                    {this.makeBoardColumns()}
                </div>
            );
        }
    }

    /**
     * ■ Gameコンポーネント
     * ┗ OとXを埋めることになるプレースホルダーを持ったゲームボードをレンダリングする
     **/
    class Game extends React.Component {
        constructor() {
            super();
            this.state = {
                history: [{
                    squares: Array(9).fill(null)
                }],
                xIsNext: true,  // 開始時の状態(Xから開始)
                stepNumber: 0 //
            };
        }

        handleClick(i) {
            var history = this.state.history;
            var current = history[history.length - 1];
            // var current = ;
            const squares = current.squares.slice();

            // すでにゲームが終了しているか、すでにマスが埋められている場合、処理を無視
            if (calculateWinner(squares) || squares[i]) {
                return;
            }
            squares[i] = this.state.xIsNext ? 'X' : 'O';

            // 状態を更新する
            this.setState({
                // 「○ or ×」を描画
                stepNumber: history.length,

                // 履歴を描画
                history: history.concat([{
                    squares: squares
                }]),

                // 次のプレイヤーを反転してセット
                xIsNext: !this.state.xIsNext,
            });
        }

        // タイムトラベル(指定された番まで戻る)
        jumpTo(step) {
            this.setState({
                stepNumber: step,
                xIsNext: (step % 2) ? false : true,
            });
        }

        render() {
            const history = this.state.history;
            // const current = history[history.length - 1];
            const current = history[this.state.stepNumber];
            const winner = calculateWinner(current.squares);

            let status;
            if (winner) {
                status = 'Winner: ' + winner;
            } else {
                status = 'Next player: ' + (this.state.xIsNext ? 'X' : 'O');
            }

            //
            const moves = history.map((step, move) => {
                const desc = move ?
                'Move #' + move :
                    'Game start';
                return (
                    // レンダリングを行う際、子コンポーネントが更新されたことを判断するため、
                    // 重複しない一意な値として、Reactの予約語である「key」プロパティをセットする
                    <li key={move}>
                        <a href="#" onClick={() => this.jumpTo(move)}>{desc}</a>
                    </li>
                );
            });

            return (
                <div className="game">
                    <div className="game-board">
                        <Board
                            squares={current.squares}
                            onClick={(i) => this.handleClick(i)}
                        />
                    </div>
                    <div className="game-info">
                        <div>{status}</div>
                        <ol>{moves}</ol>
                    </div>
                </div>
            );
        }
    }

// ========================================

    ReactDOM.render(
        <Game />,
        document.getElementById('tutorial_container')
    );

    function calculateWinner(squares) {
        const lines = [
            [0, 1, 2],
            [3, 4, 5],
            [6, 7, 8],
            [0, 3, 6],
            [1, 4, 7],
            [2, 5, 8],
            [0, 4, 8],
            [2, 4, 6],
        ];
        for (let i = 0; i < lines.length; i++) {
            const [a, b, c] = lines[i];
            if (squares[a] && squares[a] === squares[b] && squares[a] === squares[c]) {
                return squares[a];
            }
        }
        return null;
    }

});