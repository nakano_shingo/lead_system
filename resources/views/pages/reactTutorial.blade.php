@extends('layout')

@section('content')

    <h3>React.js Tutorial (tic-tac-toe)</h3>

    <div class="abstract">
        公式のチュートリアルを途中までやってみました。<br>
        皆さんの参考になればと思い、こちらにも情報を残しておきます。<br>
        <br>

        <ur>
            <li><a href="https://facebook.github.io/react/tutorial/tutorial.html">公式チュートリアルページ</a></li>
            <li><a href="http://mae.chab.in/archives/2943">チュートリアル日本語訳</a></li>
            <li><a href="https://codepen.io/nakano_shingo/pen/rWmpLo?editors=0010">Code Pen(中野編集)</a></li>
        </ur>
    </div>

    <div class="main">
        <h3>※ 以下、成果物</h3>
        <div id="errors" style="
          background: #c00;
          color: #fff;
          display: none;
          margin: -20px -20px 20px;
          padding: 20px;
          white-space: pre-wrap;
        "></div>

        <div id="tutorial_container"></div>

        <script>
            window.addEventListener('mousedown', function(e) {
                document.body.classList.add('mouse-navigation');
                document.body.classList.remove('kbd-navigation');
            });
            window.addEventListener('keydown', function(e) {
                if (e.keyCode === 9) {
                    document.body.classList.add('kbd-navigation');
                    document.body.classList.remove('mouse-navigation');
                }
            });
            window.addEventListener('click', function(e) {
                if (e.target.tagName === 'A' && e.target.getAttribute('href') === '#') {
                    e.preventDefault();
                }
            });
            window.onerror = function(message, source, line, col, error) {
                var text = error ? error.stack || error : message + ' (at ' + source + ':' + line + ':' + col + ')';
                errors.textContent += text + '\n';
                errors.style.display = '';
            };
            console.error = (function(old) {
                return function error() {
                    errors.textContent += Array.prototype.slice.call(arguments).join(' ') + '\n';
                    errors.style.display = '';
                    old.apply(this, arguments);
                }
            })(console.error);
        </script>


    </div>

@endsection