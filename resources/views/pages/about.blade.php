@extends('layout')

@section('content')
    <h2><span class="glyphicon glyphicon-asterisk"></span> DBMTとは？</h2>
    <ul>
        <li>Database Migration Toolの略。Webブラウザ上からDB設計を行い、それをExcelやSQLファイルとして出力してみよう！というシステムです。</li><br>
        <li>以下の点について「勉強して、理解を深めよう！」という目的もあります。</li>
        <ol>
            <li>「Git」(ソース管理)</li>
            <li>「Laravel」(最近流行りのPHPフレームワーク)</li>
            <li>「React.js」(facebookが出した、SPAな仕組みを動かすのに便利そうな？JSフレームワークワーク)</li>
        </ol>
    </ul>

    <h3><span class="glyphicon glyphicon-user"></span> Members</h3>

    @if (Auth::guest())
        ログイン後に表示されます。
    @else

        <table class="table table-bordered">
            <tr>
                <th class="text-center" style="max-width: 200px">グループ名</th>
                <th class="text-center">ユーザー名</th>
            </tr>

            @foreach ($users_data as $group_nm =>$users)
                @foreach ($users as $key => $user_nm)
                    <tr>
                        {{-- グループ名 --}}
                        @if($key == 0)
                            <td rowspan="{{count($users)}}" style="vertical-align: middle;">{{$group_nm}}</td>
                        @endif
                        {{-- ユーザ名 --}}
                        <td>{{$user_nm}}</td>
                    </tr>
                @endforeach
            @endforeach
        </table>

    @endif

@endsection