@extends('layout')


@section('content')

    @if (Auth::guest())
        ログインしてください。
    @else
        <div class="box">
            <h3><span class="glyphicon glyphicon-list-alt"></span> データベース選択</h3>

            <table id="grid-data" class="table table-condensed table-hover table-striped">
                <thead>
                <tr>
                    <th data-column-id="databases_id" data-type="numeric" data-order="asc">ID</th>
                    <th data-column-id="db_nm">DB名称</th>
                    <th data-column-id="description" data-order="desc">詳細</th>
                    <th data-column-id="set_nm">更新者</th>
                    <th data-column-id="updated_at">更新日時</th>
                    <th data-column-id="commands" data-formatter="commands" data-sortable="false">操作</th>
                </tr>
                </thead>
            </table>
            <button type="button" class="btn btn-sm btn-info">新規登録</button>
        </div>

        <form id="edit_form" action="/edit/tables" method="POST">
            {{csrf_field()}}
        </form>

        <br />

        <h3><span class="glyphicon glyphicon-user"></span> 最近の活動</h3>


    @endif


@endsection