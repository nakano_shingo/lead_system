<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

use DB;
use Log;

class Api extends Model
{

    /**
     * TOP画面のDB一覧を取得する
     * @param $params
     * @return array
     */
    public function get_databases_data($params)
    {
        // databasesテーブルの総件数を取得
        $total_cnt = DB::table('databases')->count();
        Log::debug('$total_cnt -> '.print_r($total_cnt, 1));

        // ページング用のlimit
        $limit = $params['rowCount'];
        Log::debug('$limit -> '.print_r($limit, 1));

        // ページング用のoffset
        $offset = $params['current'];
        Log::debug('$offset -> '.print_r($offset, 1));

        // ソート
        $sort_key = key($params['sort']);
        $order = $params['sort'][$sort_key];

        // データ取得
        $databases = DB::table('databases')->orderBy($sort_key, $order)->skip(($offset-1)*$limit)->take($limit)->get();

        $result = [
            'current'  => $offset,
            'rowCount' => $limit,
            'rows'     => $databases,
            'total'    => $total_cnt
        ];

        return $result;
    }

}
