<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;
use DB;
use Log;

class Pages extends Model
{
    /**
     * ユーザテーブルからユーザ情報を取得
     * @return mixed
     */
    public function get_users_info()
    {
        // usersテーブルから値を取得
        $columns = [
            'groups',
            'name',
        ];
        $users_data = DB::table('users')->select($columns)->orderBy('groups')->get();

        // 取得した値を整形
        $result = [];
        foreach ($users_data as $key => $value) {
            $result[$value->groups][] = $value->name;
        }
        Log::debug('$result -> '.print_r($result, 1));

        return $result;
    }

}
