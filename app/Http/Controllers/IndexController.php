<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class IndexController extends Controller
{
    /**
     * TOPページ用アクション
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        // ページ独自CSSの設定
        $assetCss = [
            asset('/css/index.css')
        ];

        // viewをセット
        return view('index', [
            'assetCss' => $assetCss
        ]);
    }
    //
}
