<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Log;

class EditController extends Controller
{
    /**
     * テーブル編集画面 アクション
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function tables(Request $request)
    {
        $params = $request->input();
        Log::debug('$params ->'.print_r($params, 1));

        return view('edit.tables');
    }
}
