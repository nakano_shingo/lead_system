<?php

namespace App\Http\Controllers;

use Log;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Models\Api;

class ApiController extends Controller
{
    /**
     * [Databases]テーブルから、DBデータの一覧を取得する
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getDbInfo(Request $request)
    {
        $params = $request->input();
        Log::debug('$params ->'.print_r($params, 1));

        // モデルをインスタンス化
        $apiModel = new Api();

        // データ取得
        $result = $apiModel->get_databases_data($params);

        return response()->json($result);
    }
}
