<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Models\Pages;

class PagesController extends Controller
{
    public function __construct()
    {
        $this->model = new Pages();
    }

    /**
     * DBMTとは
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function about()
    {
        // ユーザー情報を取得
        $users_data = $this->model->get_users_info();

        // viewをセット + 変数をアサイン
        return view('pages.about', compact('users_data'));
    }

    /**
     * React.js Tutorials (tic-tac-toe)
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function reactTutorial()
    {
        // ページ独自CSSの設定
        $assetCss = [
            '/lead_system/css/react_tutorial.css'
        ];
        $assetJs = [
            '/lead_system/js/react_tutorial2.js'
        ];

        // viewをセット
        return view('pages.reactTutorial',[
            'assetCss' => $assetCss,
            'assetJs'  => $assetJs,
        ]);
    }

    /**
     * お問い合わせ
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function contact()
    {
        return view('pages.contact');
    }
}
