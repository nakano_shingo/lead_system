<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

// TOPページ (DB選択画面/最近の活動など)
Route::get('/', 'IndexController@index');

// API (JSプラグインなどで利用するデータなどを提供する)
Route::post('api/getDbInfo', 'ApiController@getDbInfo');

// テーブル編集画面
Route::post('edit/tables', 'EditController@tables');

// 静的ページ
Route::get('about', 'PagesController@about');       // DBMTとは
Route::get('react/tutorial', 'PagesController@reactTutorial');   // React.jsのチュートリアル
Route::get('contact', 'PagesController@contact');   // お問い合わせ

// ログイン/ログアウト
Route::get('auth/login', 'Auth\AuthController@getLogin');
Route::post('auth/login', 'Auth\AuthController@postLogin');
Route::get('auth/logout', 'Auth\AuthController@logout');

// ユーザー登録
Route::get('auth/register', 'Auth\AuthController@getRegister');
Route::post('auth/register', 'Auth\AuthController@postRegister');


// Route::controller('auth', 'auth\AuthController');  //  Implicit Controller（暗黙コントローラ）

//Route::get('/', function () {
//    return view('welcome');
//});
//Route::get('/', function () {
//    return 'Hello World';
//});