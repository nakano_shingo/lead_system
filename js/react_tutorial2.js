"use strict";

var _slicedToArray = function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"]) _i["return"](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError("Invalid attempt to destructure non-iterable instance"); } }; }();

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

$(function () {
    /**
     * ■ Squareコンポーネント
     * ┗ React.Componentを継承するクラスを定義しない、「stateless functional components」と呼ばれるより簡易的な構文
     *   (renderメソッドのみからなるコンポーネントタイプ)
     **/
    function Square(props) {
        return React.createElement(
            "button",
            { className: "square", onClick: function onClick() {
                    return props.onClick();
                } },
            props.value
        );
    }

    /**
     * ■ Boadコンポーネント
     * ┗ 9つの四角いマスをレンダリングする
     **/

    var Board = function (_React$Component) {
        _inherits(Board, _React$Component);

        function Board() {
            _classCallCheck(this, Board);

            return _possibleConstructorReturn(this, (Board.__proto__ || Object.getPrototypeOf(Board)).apply(this, arguments));
        }

        _createClass(Board, [{
            key: "makeBoardColumns",

            // マスを生成する
            value: function makeBoardColumns() {
                // マス番号
                var squareCnt = 0;

                // 列を作成
                var columns = [];
                for (var c = 0; c < 3; c++) {
                    // 行を作成
                    var rows = [];
                    for (var r = 0; r < 3; r++) {
                        rows.push(this.renderSquare(squareCnt));
                        squareCnt++;
                    }
                    columns.push(React.createElement(
                        "div",
                        { className: "board-row", key: c },
                        rows
                    ));
                }
                return columns;
            }

            // 1マスを作成

        }, {
            key: "renderSquare",
            value: function renderSquare(i) {
                var _this2 = this;

                return React.createElement(Square, { value: this.props.squares[i], onClick: function onClick() {
                        return _this2.props.onClick(i);
                    }, key: i });
            }
        }, {
            key: "render",
            value: function render() {
                return React.createElement(
                    "div",
                    null,
                    React.createElement(
                        "div",
                        { className: "status" },
                        status
                    ),
                    this.makeBoardColumns()
                );
            }
        }]);

        return Board;
    }(React.Component);

    /**
     * ■ Gameコンポーネント
     * ┗ OとXを埋めることになるプレースホルダーを持ったゲームボードをレンダリングする
     **/


    var Game = function (_React$Component2) {
        _inherits(Game, _React$Component2);

        function Game() {
            _classCallCheck(this, Game);

            var _this3 = _possibleConstructorReturn(this, (Game.__proto__ || Object.getPrototypeOf(Game)).call(this));

            _this3.state = {
                history: [{
                    squares: Array(9).fill(null)
                }],
                xIsNext: true, // 開始時の状態(Xから開始)
                stepNumber: 0 //
            };
            return _this3;
        }

        _createClass(Game, [{
            key: "handleClick",
            value: function handleClick(i) {
                var history = this.state.history;
                var current = history[history.length - 1];
                // var current = ;
                var squares = current.squares.slice();

                // すでにゲームが終了しているか、すでにマスが埋められている場合、処理を無視
                if (calculateWinner(squares) || squares[i]) {
                    return;
                }
                squares[i] = this.state.xIsNext ? 'X' : 'O';

                // 状態を更新する
                this.setState({
                    // 「○ or ×」を描画
                    stepNumber: history.length,

                    // 履歴を描画
                    history: history.concat([{
                        squares: squares
                    }]),

                    // 次のプレイヤーを反転してセット
                    xIsNext: !this.state.xIsNext
                });
            }

            // タイムトラベル(指定された番まで戻る)

        }, {
            key: "jumpTo",
            value: function jumpTo(step) {
                this.setState({
                    stepNumber: step,
                    xIsNext: step % 2 ? false : true
                });
            }
        }, {
            key: "render",
            value: function render() {
                var _this4 = this;

                var history = this.state.history;
                // const current = history[history.length - 1];
                var current = history[this.state.stepNumber];
                var winner = calculateWinner(current.squares);

                var status = void 0;
                if (winner) {
                    status = 'Winner: ' + winner;
                } else {
                    status = 'Next player: ' + (this.state.xIsNext ? 'X' : 'O');
                }

                //
                var moves = history.map(function (step, move) {
                    var desc = move ? 'Move #' + move : 'Game start';
                    return (
                        // レンダリングを行う際、子コンポーネントが更新されたことを判断するため、
                        // 重複しない一意な値として、Reactの予約語である「key」プロパティをセットする
                        React.createElement(
                            "li",
                            { key: move },
                            React.createElement(
                                "a",
                                { href: "#", onClick: function onClick() {
                                        return _this4.jumpTo(move);
                                    } },
                                desc
                            )
                        )
                    );
                });

                return React.createElement(
                    "div",
                    { className: "game" },
                    React.createElement(
                        "div",
                        { className: "game-board" },
                        React.createElement(Board, {
                            squares: current.squares,
                            onClick: function onClick(i) {
                                return _this4.handleClick(i);
                            }
                        })
                    ),
                    React.createElement(
                        "div",
                        { className: "game-info" },
                        React.createElement(
                            "div",
                            null,
                            status
                        ),
                        React.createElement(
                            "ol",
                            null,
                            moves
                        )
                    )
                );
            }
        }]);

        return Game;
    }(React.Component);

    // ========================================

    ReactDOM.render(React.createElement(Game, null), document.getElementById('tutorial_container'));

    function calculateWinner(squares) {
        var lines = [[0, 1, 2], [3, 4, 5], [6, 7, 8], [0, 3, 6], [1, 4, 7], [2, 5, 8], [0, 4, 8], [2, 4, 6]];
        for (var i = 0; i < lines.length; i++) {
            var _lines$i = _slicedToArray(lines[i], 3),
                a = _lines$i[0],
                b = _lines$i[1],
                c = _lines$i[2];

            if (squares[a] && squares[a] === squares[b] && squares[a] === squares[c]) {
                return squares[a];
            }
        }
        return null;
    }
});
//# sourceMappingURL=react_tutorial2.js.map
