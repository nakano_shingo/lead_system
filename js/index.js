/**
 * Created by nakano_shingo on 2016/11/05.
 * index.js
 * トップページ用js
 */

$(function(){

    var grid = $("#grid-data").bootgrid({
        ajax: true,
        // ajaxSettings: {
        //     method: "GET",
        //     cache: false
        // },
        post: function ()
        {
            /* To accumulate custom parameter with the request object */
            return {
                id: "b0df282a-0d67-40e5-8558-c9e93b7befed"
            };
        },
        url: "/lead_system/api/getDbInfo",
        formatters: {
            "commands": function(column, row)
            {
                return "<button type=\"button\" class=\"btn btn-xs btn-default command-edit\" data-row-id=\"" + row.id + "\"><span class=\"glyphicon glyphicon-pencil\"></span></button> " +
                    "<button type=\"button\" class=\"btn btn-xs btn-default command-delete\" data-row-id=\"" + row.id + "\"><span class=\"glyphicon glyphicon-trash\"></span></button>";
            }
        }
    }).on("loaded.rs.jquery.bootgrid", function()
    {
        // alert(grid.bootgrid("getCurrentPage"));
        grid.find(".command-edit").on("click", function(e)
        {
            // CSRF-token
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            // 編集ボタン押下時の処理
            $('<input>').attr({
                type: 'hidden', id: 'id', value: $(this).data('row-id')
            }).appendTo('#edit_form');
            $('#edit_form').submit();

            // alert("You pressed edit on row: " + $(this).data("row-id"));
            // $('<form/>', {action: "/edit/tables", method: 'post'})
            //     .append($('<input/>', {type: 'hidden', name: 'id', value: $(this).data('row-id')}))
            //     .appendTo(document.body)
            //     .submit();
        }).end().find(".command-delete").on("click", function(e)
        {
            // 削除ボタン押下時の処理
            alert("You pressed delete on row: " + $(this).data("row-id"));
        });
    });

});
